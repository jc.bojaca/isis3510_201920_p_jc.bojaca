package com.example.bonomoviles

import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.LruCache
import android.view.View
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.*
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    /**
     * turn on the Bluetooth and start the discovery mode for 5 min
     */
    fun turnOnBluetooth(view: View) {
        val textView = findViewById<TextView>(R.id.texto)
        val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if (bluetoothAdapter != null) {
            if (!bluetoothAdapter.isEnabled) {
                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivity(enableBtIntent)
            }
            val discoverableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE)
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300)
            startActivity(discoverableIntent)

            textView.text = "El bluethooth ahora esta activado"
        } else {
            textView.text = "El dispositivo no cuenta con bluethooth"
        }
    }

    fun checkAttendance(view: View) {
        val textView = findViewById<TextView>(R.id.texto)
        val url = getUrlAttendance("201613162")

        println(url)
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.GET, url, null,
            Response.Listener { response ->
                val res = response.getJSONObject("fields").getJSONObject("name").getString("stringValue")
                textView.text = res + " esta en clase"
            },
            Response.ErrorListener { error ->
                textView.text = "no se encuentra en clase: \n" + url
            }
        )

        val requestQueue = LazyRequestQueue.getInstance(this.applicationContext).requestQueue
        requestQueue.add(jsonObjectRequest)
    }

    /**
     * return the http url to check the today attendance
     */
    private fun getUrlAttendance(userCode: String): String {
        val formatter = SimpleDateFormat("dd-MM-yyyy")
        val dateArray = formatter.format(Date()).toString().split("-").toTypedArray()
        val mes = dateArray[1].toInt() - 1
        return "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/" + dateArray[0] + "-" + mes + "-" + dateArray[2] + "/students/" + userCode
    }

    /**
     * request queue to http functions
     */
    class LazyRequestQueue constructor(context: Context) {
        companion object {
            @Volatile
            private var INSTANCE: LazyRequestQueue? = null

            fun getInstance(context: Context) =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: LazyRequestQueue(context).also {
                        INSTANCE = it
                    }
                }
        }

        val imageLoader: ImageLoader by lazy {
            ImageLoader(requestQueue,
                object : ImageLoader.ImageCache {
                    private val cache = LruCache<String, Bitmap>(20)
                    override fun getBitmap(url: String): Bitmap {
                        return cache.get(url)
                    }

                    override fun putBitmap(url: String, bitmap: Bitmap) {
                        cache.put(url, bitmap)
                    }
                })
        }
        val requestQueue: RequestQueue by lazy {
            Volley.newRequestQueue(context.applicationContext)
        }

        fun <T> addToRequestQueue(req: Request<T>) {
            requestQueue.add(req)
        }
    }
}
